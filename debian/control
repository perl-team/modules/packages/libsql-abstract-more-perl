Source: libsql-abstract-more-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: gregor herrmann <gregoa@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libmodule-build-perl
Build-Depends-Indep: liblist-moreutils-perl <!nocheck>,
                     libmro-compat-perl <!nocheck>,
                     libnamespace-clean-perl <!nocheck>,
                     libparams-validate-perl <!nocheck>,
                     libsql-abstract-classic-perl <!nocheck>,
                     libsql-abstract-perl <!nocheck>,
                     libtest-deep-perl <!nocheck>,
                     libtest-exception-perl <!nocheck>,
                     libtest-pod-perl <!nocheck>,
                     perl
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libsql-abstract-more-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libsql-abstract-more-perl.git
Homepage: https://metacpan.org/release/SQL-Abstract-More
Rules-Requires-Root: no

Package: libsql-abstract-more-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libmro-compat-perl,
         libnamespace-clean-perl,
         libparams-validate-perl,
         libsql-abstract-classic-perl | libsql-abstract-perl
Description: extension of SQL::Abstract with more constructs and more flexible API
 Sql::Abstract::More generates SQL from Perl datastructures. This is a
 subclass of SQL::Abstract, fully compatible with the parent class, but with
 some additions:
 .
 1) additional SQL constructs like -union, -group_by, join, etc;
 .
 2) methods take arguments as named parameters instead of positional
 parameters, so that various SQL fragments are more easily identified;
 .
 3) values passed to select, insert or update can directly incorporate
 information about datatypes, in the form of arrayrefs of shape [$value,
 \%type].
